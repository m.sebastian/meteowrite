import axios from "axios";
const defaultLink = "https://reqres.in/api"
export  function registerRequest(data) {
    return axios({
        method: 'post',
        url: `${defaultLink}/register`,
        data: {
            "email": "eve.holt@reqres.in",
            "password": "pistol"
        }
    });
}
export function updateUser(data) {
    return axios({
        method: 'put',
        url: `${defaultLink}/users/2`,
        data: {
            "name": "morpheus",
            "job": "zion resident"
        }
    });
}

export function login(data) {
    return axios({
        method: 'post',
        url: `${defaultLink}/login`,
        data: {
            "email": "eve.holt@reqres.in",
            "password": "cityslicka"
        }
    });
}
