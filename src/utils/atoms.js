import {atom} from "recoil";

export const recoverEmail = atom({
    key: 'recoverEmail',
    default: '',
});
