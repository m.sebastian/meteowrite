import Card from "../../components/Card";
import Input from "../../components/Input";
import Button from "../../components/Button";
import {useState} from 'react'
import {updateUser} from "../../utils/api";
import {useHistory} from "react-router-dom";

const BasicInformation = () => {
    let history = useHistory();

    const [data, setData] = useState({firstName: "", lastName: "", company: "", role: ""});

    const validateForm = () => {
        return data.firstName.length !== 0 || data.lastName.length !== 0 || data.company.length !== 0 || data.role.length !== 0
    }

    const onSubmit = async () => {
        const isValidForm = validateForm();
        if (!isValidForm) {
            return;
        }
        try {
            let request = await updateUser(data);
            if (request.status === 200) {
                history.push('/verification-link')
            }
        } catch (e) {
            console.log(e)
        }
    }


    return (
        <>
            <Card child={<>
                <div className={"basicInformationWrapper"}>
                    <h1>Basic information</h1>
                    <p>This is a placeholder description of why we need to know this type of information.</p>
                </div>
                <div>
                    <Input htmlFor={'firstName'} type={'text'} label={"First Name"}
                           onChange={(e) => setData({...data, firstName: e.target.value})}/>
                    <Input htmlFor={'lastName'} type={'text'} label={"Last Name"}
                           onChange={(e) => setData({...data, lastName: e.target.value})}/>
                    <Input htmlFor={'firstName'} type={'text'} label={"Company"}
                           onChange={(e) => setData({...data, company: e.target.value})}
                           placeholder={"Where are you working right now?"}/>
                    <Input htmlFor={'firstName'} type={'text'} label={"Company Role"}
                           onChange={(e) => setData({...data, role: e.target.value})}
                           placeholder={"What is your role in the company?"}/>
                </div>
                <div className={"signUpButtonWrapper"}>
                    <Button text={"Sign up"} onClick={onSubmit}/>
                </div>
            </>}/>
        </>
    )
};
export default BasicInformation;
