import Card from "../../components/Card";
import Input from "../../components/Input";
import Button from "../../components/Button";
import {useState} from 'react';
import {validateEmail} from "../../utils/validators";
import {recoverEmail} from "../../utils/atoms";
import {
    useRecoilState
} from 'recoil';
import { useHistory } from "react-router-dom";

const ResetPassword = () => {
    let history = useHistory();

    const [email, setEmail] = useRecoilState(recoverEmail);
    const [emailError, setEmailError] = useState(false);


    const onSumbit = () => {
        if(email.length === 0 || emailError) {
            return;
        }
        history.push('/password-sent');
    }

    return <>
        <Card child={<>
            <div className={"basicInformationWrapper"}>
                <h4>Reset your password</h4>
                <p>Enter your Meteowrite.io email adress so we can reset your password.</p>
            </div>
            <div className={"margin40"}>
                <Input
                    placeholder={"Your email"}
                    onChange={(e) => setEmail(e.target.value)}
                    label={"Email"}
                    type={'email'}
                    htmlFor={"email"}
                    onBlur={() => setEmailError(!validateEmail(email))}
                    error={emailError}
                    errorMessage={"email error validation"}
                />
            </div>
            <div className={"nextButtonWrapper"}>
                <Button text={"NEXT"} onClick={onSumbit} />
            </div>
            <div className={"rememberPass"}>
                <p>Remember password? <span onClick={() => history.goBack()}>Go back</span></p>
            </div>
        </>}/>
    </>
}
export default ResetPassword;
