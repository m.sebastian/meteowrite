import Card from "../../components/Card";
import Button from "../../components/Button";
import {useRecoilValue} from "recoil";
import {recoverEmail} from "../../utils/atoms";
import { useHistory } from "react-router-dom";

const PasswordSent = () => {
    let history = useHistory();

    const onSumbit = () => {
        history.push('/new-password')
    }

    const email = useRecoilValue(recoverEmail);

    console.log(email);
    return <>
        <Card child={<>
            <div className={"basicInformationWrapper"}>
                <h4>Password sent</h4>
                <p>An email has been sent to <span className={'bold'}>{email}</span>. If this email adress is registered to Meteowrite.io, you’ll
                    recieve instructions on how to set a new password.</p>
            </div>
            <div className={"margin40"}>
                <Button onClick={onSumbit} text={"Enter new password"}/>
            </div>
            <div className={"didntGetEmail"}>
                <p>Didn’t get an email?</p>
            </div>
        </>}/>
    </>
}
export default PasswordSent;
