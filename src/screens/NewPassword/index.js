import Card from "../../components/Card";
import Input from "../../components/Input";
import Button from "../../components/Button";
import {useState} from 'react'
import {validatePassword} from "../../utils/validators";
import {useHistory} from "react-router-dom";
const NewPassword = () => {
    let history = useHistory();

    const [data, setData] = useState({password: "", confirmPassword: ""});
    const [errors, setErrors] = useState({password: false, confirmPassword: false})

    const onSubmit = () => {
        if(data.password.length === 0 || data.confirmPassword.length === 0) {
            return;
        }
        if(errors.confirmPassword || errors.password) {
            return;
        }
        history.push('/login')
    }


    return <>
        <Card child={<>
            <div className={"choosePassTitle"}>
                <h4>Choose your new password</h4>
            </div>
            <div className={"choosePassInputs"}>
                <Input
                    htmlFor={'password'}
                    type={"password"}
                    label={"Password"}
                    onChange={(e) => setData({...data, password: e.target.value})}
                    placeholder={""}
                    onBlur={() => setErrors({...errors, password: !validatePassword(data.password)})}
                    errorMessage={"password validation message"}
                    error={errors.password}
                />
                <Input
                    placeholder={""}
                    label={"Confirm Password"}
                    type={'password'}
                    htmlFor={"confirmPass"}
                    onChange={(e) => setData({...data, confirmPassword: e.target.value})}
                    onBlur={() => setErrors({...errors, confirmPassword: data.password !== data.confirmPassword})}
                    errorMessage={"password validation message"}
                    error={errors.confirmPassword}
                />
            </div>
            <div className={"nextButtonWrapper"}>
                <Button text={"Submit"} onClick={onSubmit}/>
            </div>
        </>}/>
    </>
}
export default NewPassword
