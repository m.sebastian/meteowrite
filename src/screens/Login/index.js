import Card from "../../components/Card";
import Input from "../../components/Input";
import Button from "../../components/Button";
import {useState} from 'react';
import {login} from "../../utils/api";
import ThirdPartyAuth from "../../components/ThirdPartyAuth";
import { useHistory } from "react-router-dom";

const Login = () => {
    let history = useHistory();

    const [data, setData] = useState({username: "", password: "", rememberMe: false});

    const onSubmit = async () => {
        if (data.username.length === 0 || data.password.length === 0) {
            return;
        }
        try {
            let res = await login(data)
            if (res.status === 200) {
                alert("Success!")
            }
        } catch (e) {
            console.log(e)
        }
    }

    return <>
        <Card child={<>
            <div className={"formName"}>
                <h1>Login</h1>
            </div>
            <ThirdPartyAuth text={"Login with linkedin"} />
            <div>
                <h5 className={"or"}>or</h5>
            </div>
            <div>
                <Input
                    placeholder={""}
                    error={false}
                    errorMessage={"validation error"}
                    onChange={(e) => setData({...data, username: e.target.value})}
                    htmlFor={'email'}
                    type={'email'}
                    label={"email"}
                    value={data.username}
                />
                <Input
                    placeholder={""}
                    error={false}
                    errorMessage={"validation error"}
                    onChange={(e) => setData({...data, password: e.target.value})}
                    htmlFor={'password'}
                    type={'password'}
                    label={"password"}
                    value={data.password}
                />
            </div>
            <div className={"displayInLine"}>
                <div className={"displayInLine"}>
                    <button className={"yesButton"} onClick={() => setData({
                        ...data,
                        rememberMe: !data.rememberMe
                    })}>{data.rememberMe ? "NO" : "YES"}</button>
                </div>
                <div className={"displayInLine"}>
                    <h6>Remember me</h6>
                </div>
                <div className={"displayInLine"}>
                    <p onClick={() => history.push("/reset-password")}>Forgot password?</p>
                </div>
            </div>
            <div className={"nextButtonWrapper"}>
                <Button text={'Login'} onClick={onSubmit}/>
            </div>
            <div className={"newToMeteowrite"}>
                <p>New to meteowrite? <span onClick={() => history.push("/register")}>Register</span></p>
            </div>
        </>}/>
    </>
}
export default Login;
