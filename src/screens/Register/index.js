
import Input from "../../components/Input";
import Button from "../../components/Button";
import Card from "../../components/Card";
import {validateEmail, validatePassword} from "../../utils/validators";
import {useState} from 'react';
import {registerRequest} from "../../utils/api";
import ThirdPartyAuth from "../../components/ThirdPartyAuth";
import {useHistory} from "react-router-dom";

const Register = () => {
    let history = useHistory();

    const [errors, setErrors] = useState({email: false, password: false, confirmPassword: false});
    const [data, setData] = useState({email: "", password: "", confirmPassword: ""});
    const [agreeTerms, setAgreeTerms] = useState(false)
    const validateForm = () => {
        let isValidForm = true;
        Object.keys(errors).forEach(key => {
            if(errors[key] === true) {
                isValidForm = false;
            }
        })
        return isValidForm;
    }

    const emptyFields = () => {
        return data.email.length === 0 || data.password.length === 0 || data.confirmPassword.length === 0
    }

    const onSubmit = async () => {
        let isValidForm = validateForm();
        if(!isValidForm) {
            return;
        }
        if(emptyFields()) {
            return;
        }
        if(!agreeTerms) {
            alert("Please accept our Terms and conditions...")
            return;
        }
        try {
            let request = await registerRequest({email: data.email, password: data.password})
            if(request.status === 200) {
                history.push('/basic-information')
            }
        } catch (e) {
            console.log(e);
        }
    }

    return (
        <>
            <Card child={<>
                <div className={"formName"}>
                    <h1 className={"register"}>Register</h1>
                </div>
                <div className={"externalAuth"}>
                    <ThirdPartyAuth text={"Continue with Linkedin"} />
                    <h5 className={"or"}>or</h5>
                </div>
                <Input
                    type={'email'}
                    onChange={(e) => setData({...data, email: e.target.value})}
                    label={"Email"}
                    htmlFor={"email"}
                    onBlur={() => setErrors({...errors, email: !validateEmail(data.email)})}
                    error={errors.email}
                    errorMessage={"email validation message"}
                />
                <Input
                    type={'password'}
                    onChange={(e) => setData({...data, password: e.target.value})}
                    label={"Password"}
                    htmlFor={"password"}
                    onBlur={() => setErrors({...errors, password: !validatePassword(data.password)})}
                    error={errors.password}
                    errorMessage={"password validation message"}
                />
                <Input
                    type={'password'}
                    onChange={(e) => setData({...data, confirmPassword: e.target.value})}
                    label={"Confirm Password"}
                    htmlFor={"confirmPass"}
                    onBlur={() => setErrors({...errors, confirmPassword: data.password !== data.confirmPassword})}
                    error={errors.confirmPassword}
                    errorMessage={"confirmPass validation message"}
                />
                <div className={"agreeTermsWrapper"}>
                    <button className={"yesButton"} onClick={() =>setAgreeTerms(!agreeTerms)}>{agreeTerms ? "NO" : "YES"}</button>
                    <p className={"agreeTerms"}>I agree to the Meteowrite<span> Terms and Conditions.</span></p>
                </div>
                <div className={"createAccountWrapper"}>
                    <Button onClick={onSubmit} text={"Create Account"}/>
                </div>
                <div className={"alreadyHaveAccountWrapper"}>
                    <p>Already have an account?<span onClick={() => history.push('/login')}>Login</span></p>
                </div>
            </>}
            />
        </>
    )
};
export default Register;
