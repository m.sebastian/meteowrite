import Card from "../../components/Card";
import verificationLink from '../../assets/images/verification-state-img.svg'
import Button from "../../components/Button";
import { useHistory } from "react-router-dom";

const VerificationLink = () => {
    let history = useHistory();

    return (
        <Card child={<>
            <div className={"verificationLinkWrapper"}>
                <img src={verificationLink} alt={"Verification link photo"} className={"verificationLinkImage"}/>
                <h1>A verification link has been sent to your email account</h1>
                <p>Please click on the link that has just been sent to your email account to verify your email and
                    continue the registration process.</p>
            </div>
            <div className={"loginButtonWrapper"}>
                <Button onClick={() => history.push('/login')} text={"Log in"} />
            </div>
            <div className={"didntGetEmail"}>
                <p>Didn’t get an email?</p>
            </div>
        </>}/>
    )
};
export default VerificationLink;
