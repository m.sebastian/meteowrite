import './App.css';
import Register from "./screens/Register";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
} from "react-router-dom";
import BasicInformation from "./screens/BasicInformation";
import VerificationLink from "./screens/VerificationLink";
import ResetPassword from "./screens/ResetPassword";
import PasswordSent from "./screens/PasswordSent";
import NewPassword from "./screens/NewPassword";
import Login from "./screens/Login";
import {
    RecoilRoot,
} from 'recoil';


function App() {
    return (
        <RecoilRoot>
        <div className="App">

                <Router>
                    <Switch>
                        <Route exact path="/">
                            <Redirect to="/register"/>
                        </Route>
                        <Route exact path="/register">
                            <Register/>
                        </Route>
                        <Route exact path="/basic-information">
                            <BasicInformation/>
                        </Route>
                        <Route exact path="/verification-link">
                            <VerificationLink/>
                        </Route>
                        <Route exact path="/reset-password">
                            <ResetPassword/>
                        </Route>
                        <Route exact path="/password-sent">
                            <PasswordSent/>
                        </Route>
                        <Route exact path="/new-password">
                            <NewPassword/>
                        </Route>
                        <Route exact path="/login">
                            <Login/>
                        </Route>
                    </Switch>
                </Router>
        </div>
        </RecoilRoot>
    );
}

export default App;
