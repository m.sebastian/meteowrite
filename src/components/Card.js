import Logo from "../assets/images/logo.png";;

const Card = ({child}) => {
    return (
        <>
            <div className="cardWrapper">
                <div className="card">
                    <div className="logo">
                        <img src={Logo} alt={"meteowrite logo"} className={"logoImg"}/>
                    </div>
                    <>
                        {child}
                    </>
                </div>
            </div>
        </>
    )
}
export default Card;
