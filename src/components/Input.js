const Input = ({label, onChange, placeholder, type,htmlFor, error,value, errorMessage, ...rest}) => {
    return (
        <div className={"marginInputs"}>
            <div className={"inputWrapper"}>
                <label className={"inputPlaceholder"} htmlFor={htmlFor}>{label}</label>
                <div>
                    <input value={value} className={"inputElement"} id={htmlFor} {...rest} placeholder={placeholder} type={type} onChange={onChange}/>
                </div>
            </div>
            {error && <p>{errorMessage}</p>}
        </div>
    )
}
export default Input
