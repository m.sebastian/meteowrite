import {LinkedIn} from "react-linkedin-login-oauth2";
import LinkedinButton from "./LinkedInButton";

const ThirdPartyAuth = ({text}) => {
    const handleSuccess = (data) => {
        console.log({
            code: data.code,
            errorMessage: '',
        });
    };

    const handleFailure = (error) => {
        console.log({
            code: '',
            errorMessage: error.errorMessage,
        });
    };

    return <>
        <div>
            <LinkedIn
                clientId="81lx5we2omq9xh"
                onFailure={handleFailure}
                onSuccess={handleSuccess}
                redirectUri="http://localhost:3000/linkedin"
                renderElement={() => <LinkedinButton text={text}/>}
            />
        </div>
    </>
}
export default ThirdPartyAuth
