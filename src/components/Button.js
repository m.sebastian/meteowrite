const Button = ({text, onClick}) => {
    return (
        <>
            <div>
                <button className={'starfieldButton'} onClick={onClick} type={'button'}>
                    {text}
                </button>
            </div>
        </>
    )
}
export default Button;
