import linkedinImage from "../assets/images/linkedin-icon.svg";

const LinkedinButton = ({text}) => {
    return <>
        <div className={"linkedinButton"}>
            <button><img src={linkedinImage}/><span>{text}</span></button>
        </div>
    </>
}
export default LinkedinButton
